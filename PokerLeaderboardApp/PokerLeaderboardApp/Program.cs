﻿using System;
using System.IO;
using System.Threading;
using PokerLeaderboardApp.Data;
using PokerLeaderboardApp.Repository;
using PokerLeaderboardApp.UI;

namespace PokerLeaderboardApp
{
    class Program
    {
        static void Main(string[] args)
        {

            //TODO: comments
            //TODO: review all code
            //TODO: unit tests
            //TODO: more error checking
            //TODO: improve output formatting
            //TODO: remove unused imports
            if (args.Length < 1)
            {
                ShowUsage();
                return;
            }

            // Get data file location from command line and validate
            var dataFolder = args[0].Trim();
            if (!ValidateDataFolder(dataFolder))
            {
                return;
            }

            // Create a PlayerRepository and verify the data file
            var playerRepository = new PlayerRepository(new FileHandler(), dataFolder);
            
            if (!playerRepository.VerifyDataFileExists())
            {
                Console.WriteLine($"Data file ({playerRepository.FullDataFilePath}) not found, will initialize with empty data set.  Continue (Y or N)?");
                var key = Console.ReadKey().KeyChar.ToString().ToUpper();
                
                if (!key.Equals("Y"))
                    return;

                playerRepository.InitializeData();
            }

            // Begin processing with UI
            new UIProcessor(playerRepository).ProcessInput();
        }

        private static bool ValidateDataFolder(string dataFolder)
        {
            dataFolder = dataFolder.Replace('\\', Path.DirectorySeparatorChar);
            dataFolder = dataFolder.Replace('/', Path.DirectorySeparatorChar);

            if (Directory.Exists(dataFolder)) return true;
            
            Console.WriteLine("Invalid data folder specified.");
            Thread.Sleep(2000);
            return false;
        }

        private static void ShowUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("  PokerLeaderboardApp <Data Folder>");
            Console.ReadKey();
        }
    }
}
