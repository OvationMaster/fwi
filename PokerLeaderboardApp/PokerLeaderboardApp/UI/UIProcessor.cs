﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using PokerLeaderboardApp.Data;
using PokerLeaderboardApp.Entity;
using PokerLeaderboardApp.Extensions;
using PokerLeaderboardApp.Repository;

namespace PokerLeaderboardApp.UI
{
    public class UIProcessor
    {
        private readonly IDataRepository<Player> _dataRepository;

        public UIProcessor (IDataRepository<Player> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        public void ProcessInput()
        {
            do
            {
                ShowData();
            } while (ProcessUserSelection());
        }

        private static void DisplayMenu()
        {
            Console.WriteLine();
            Console.Write("Enter one of the following options: (A)dd Player, (R)emove Player, (E)dit Player Winnings, (C)lear Data, (Q)uit");
        }

        private bool ProcessUserSelection()
        {
            string[] validSelections = { "A", "R", "E", "C", "Q" };

            string key;
            while(true)
            {
                DisplayMenu();
                key = Console.ReadKey().KeyChar.ToString().ToUpper();
                if (validSelections.Any(s => s.Equals(key)))
                    break;
                Console.WriteLine("--INVALID SELECTION--");
            }

            Console.WriteLine();

            // We have a selection, now process accordingly
            switch (key)
            {
                case "A":
                    AddPlayer();
                    break;
                case "R":
                    RemovePlayer();
                    break;
                case "E":
                    EditPlayer();
                    break;
                case "C":
                    ClearData();
                    break;
                case "Q":
                    return false;
                default:
                    break;
            }

            return true;
        }

        private void AddPlayer()
        {
            // Get player name and winnings from user
            Console.Write($"Enter Player Name: ");
            var name = Console.ReadLine();
            if (name.Length < 1)
            {
                Console.WriteLine("-- Player name must be entered --");
                Thread.Sleep(2000);
                return;
            }
            
            Console.Write($"Enter Winings: ");
            var val = Console.ReadLine();
            if (val.Length < 1)
            {
                Console.WriteLine("-- Player winnings must be entered --");
                Thread.Sleep(2000);
                return;
            }

            // Attempt to parse the value entered to a decimal value
            if (TryValidateWinningsEntry(val, out decimal? winnings))
            {
                // Get data from repository and add a new Player using values entered
                var data = _dataRepository.GetData();
                data.Add(new Player() { Name = name, Winnings = winnings.Value, Id = data.GetNextPlayerId() });
                _dataRepository.SaveData(data);
            }
        }

        private void ClearData()
        {
            // Erase all data by calling InitializeData
            _dataRepository.InitializeData();
        }

        private void RemovePlayer()
        {
            var data = _dataRepository.GetData();
            var player = GetPlayerFromInput(data);
            if (player == null)
                return;

            data.Remove(player);
            _dataRepository.SaveData(data);
        }

        private void EditPlayer()
        {
            var data = _dataRepository.GetData();
            var player = GetPlayerFromInput(data);
            if (player == null)
                return;

            Console.Write($"Enter winnings amount for {player.Name}: ");
            var s = Console.ReadLine();
            if (TryValidateWinningsEntry(s, out decimal? winnings))
            { 
                player.Winnings = winnings.Value;
                _dataRepository.SaveData(data);
            };

            return;
        }

        private Player GetPlayerFromInput(List<Player> players)
        {
            Console.Write($"Enter Player ID: ");
            var id = Console.ReadLine();
            Player player = null;
            TryValidatePlayerIdEntry(id, players, ref player);
            return player;
        }
        
        private static bool TryValidateWinningsEntry (string enteredValue, out decimal? winningsValue)
        {
            const string errorMessage = "-- Invalid value entered, must enter a valid DOLLAR value between -1,000,000.00 and 1,000,000.00";
            if (!enteredValue.TryValidateCurrencyValue(out winningsValue, -1000000, 1000000))
            {
                Console.WriteLine(errorMessage);
                Thread.Sleep(4000);
                winningsValue = null;
                return false;
            }

            return true;
        }

        private static bool TryValidatePlayerIdEntry (string enteredId, IEnumerable<Player> players, ref Player playerFound)
        {
            var id = 0;
            try
            {
                id = int.Parse(enteredId);    
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Id must be numeric.");
                Thread.Sleep(2000);
                return false;
            }

            var pl = players?.Where(p => p.Id == int.Parse(enteredId));
            if (pl == null || !pl.Any())
            {
                Console.WriteLine($"--Invalid Player ID Entered.--");
                Thread.Sleep(2000);
                return false;
            }
            playerFound = pl.First();

            return true;
        }

        private void ShowData()
        {
            // Retrieve data
            var data = _dataRepository.GetData();

            // Display header information
            WriteDataHeader(data);

            // Display data detail lines)
            if (data != null && data.Count > 0)
            {
                data.ForEach(WriteDataLine);
            }

            Console.WriteLine();
        }

        private static void WriteDataHeader(IReadOnlyCollection<Player> data)
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("LEADERBOARD:");
            if (data == null || data.Count < 1)
            {
                Console.WriteLine($"\t-- NO DATA FOUND --");
                return;
            }
            var mean = data.CalculateMean();
            var median = data.CalculateMedian();

            data.SetPlayerClosestToMean(mean);
            data.SetPlayerClosestToMedian(median);

            Console.WriteLine($"\tMean Winnings:   ${mean,10:n}");
            Console.WriteLine($"\tMedian Winnings: ${median,10:n}");
            Console.WriteLine();
            //Console.WriteLine($"\tPlayer ID\tName\t\t\tWinnings\tClosest to Mean\tClosest to Median");
            Console.WriteLine(
                $"{"Player ID",-15}{"Name",-23}{"Winnings",10}{"Closest to Mean",25}{"Closest to Median",20}");

        }

        private static void WriteDataLine(Player p)
        {
            var cm = p.ClosestToMean ? "Yes" : "";
            var cmed = p.ClosestToMedian ? "Yes" : "";
            var s = $"{p.Id,-15}{p.Name,-23}$ {p.Winnings,10:n}{cm,20}{cmed,16}";
            Console.WriteLine(s);
        }
    }
}
