﻿using System.IO;

namespace PokerLeaderboardApp.Data
{
    // NOTE: created this class primarily to help with unit testing

    internal class FileHandler : IFileHandler
    {
        public byte[] ReadAllBytes(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }

        public void WriteAllBytes(string filePath, byte[] data)
        {
            File.WriteAllBytes(filePath, data);
        }

        public bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }
    }
}
