﻿namespace PokerLeaderboardApp.Data
{
    public interface IFileHandler
    {
        byte[] ReadAllBytes(string filePath);

        void WriteAllBytes(string filePath, byte[] data);

        bool FileExists(string filePath);
    }
}
