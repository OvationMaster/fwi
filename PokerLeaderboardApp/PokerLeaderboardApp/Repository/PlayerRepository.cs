﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using PokerLeaderboardApp.Data;
using PokerLeaderboardApp.Entity;

namespace PokerLeaderboardApp.Repository
{
    public class PlayerRepository : IDataRepository<Player>
    {
        private const string DataFileName = "PlayerWinnings.json";
        private readonly IFileHandler _fileHandler;

        public PlayerRepository(IFileHandler fileHandler, string dataFolder)
        {
            _fileHandler = fileHandler;

            // Reformat dataFolder passed-in if necessary
            // Make sure path separators are correct
            dataFolder = dataFolder.Replace('\\' , Path.DirectorySeparatorChar);
            dataFolder = dataFolder.Replace('/', Path.DirectorySeparatorChar);
            dataFolder = dataFolder.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.OrdinalIgnoreCase) ? dataFolder : dataFolder + Path.DirectorySeparatorChar;
          
            // Append the data file name to the path
            FullDataFilePath = $"{dataFolder}{DataFileName}";
        }

        public string FullDataFilePath { get; }

        public void InitializeData()
        {
            // Call SaveData with null to clear/initialize data
            SaveData(null);
        }

        public List<Player> GetData()
        {
            // Read all bytes from data file
            var winningsData = _fileHandler.ReadAllBytes(FullDataFilePath);

            // Convert bytes to string
            var winningsJson = System.Text.Encoding.Default.GetString(winningsData);

            // Deserialize Json to list of Players
            var playerList = JsonConvert.DeserializeObject<List<Player>>(winningsJson);

            return playerList;
        }

        public void SaveData(List<Player> players)
        {
            if (players == null)
            {
                players = new List<Player>();
            }
            
            // Serialize player list to Json
            var playersJson = JsonConvert.SerializeObject(players);

            // Write the Json to data file
            _fileHandler.WriteAllBytes(FullDataFilePath, Encoding.ASCII.GetBytes(playersJson));
        }

        public bool VerifyDataFileExists()
        {
            return _fileHandler.FileExists(FullDataFilePath);
        }
    }
}
