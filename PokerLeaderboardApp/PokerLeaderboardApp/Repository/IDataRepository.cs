﻿using System.Collections.Generic;

namespace PokerLeaderboardApp.Repository
{
    public interface IDataRepository<T>
    {
        List<T> GetData();

        void SaveData(List<T> data);

        void InitializeData();
    }
}
