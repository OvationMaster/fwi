﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerLeaderboardApp.Entity;

namespace PokerLeaderboardApp.Extensions
{
    public static class PlayerExtensions
    {
        private static IEnumerable<Player> WinningsClosestToValue(IEnumerable<Player> players, decimal targetValue)
        {
            if (players == null || !players.Any())
                return null;

            targetValue = targetValue.TruncateForCurrency();

            // Calculate the lowest difference between winnings and targetValue
            var minDiff = Math.Abs(targetValue - players.OrderBy(p => Math.Abs(targetValue - p.Winnings)).First().Winnings);

            // Find all players that satisfy the minimum difference
            return players.Where(p => Math.Abs(p.Winnings - targetValue) == minDiff);
        }

        public static void SetPlayerClosestToMean(this IEnumerable<Player> players, decimal meanValue)
        {
            // Get list of players that are closest to the mean
            var p = WinningsClosestToValue(players, meanValue);
            if (p == null) return;

            // Set the ClosestToMean flag for each player found
            foreach (var player in p)
                player.ClosestToMean = true;
        }

        public static void SetPlayerClosestToMedian(this IEnumerable<Player> players, decimal medianValue)
        {
            // Get a list of players that are closest to the median
            var p = WinningsClosestToValue(players, medianValue);
            if (p == null) return;

            // Set the ClosestToMedian flag for each player found
            foreach (var player in p)
                player.ClosestToMedian = true;
        }

        public static int GetNextPlayerId(this IEnumerable<Player> players)
        {
            // Determine what the next player ID should be, based on highest current ID value
            return players == null || !players.Any() ? 1 : players.Max(p => p.Id) + 1;
        }

        public static decimal CalculateMean(this IEnumerable<Player> players)
        {
            // Get array of decimal values from player winnings
            var numbers = players?.Select(p => p.Winnings).ToArray();
            
            // Get average of all winnings values
            return numbers == null || !numbers.Any() ? 0 : numbers.Average().TruncateForCurrency();
        }

        public static decimal CalculateMedian(this IEnumerable<Player> players)
        {
            // Get array of decimal values from player winnings
            var numbers = players?.Select(p => p.Winnings).ToArray();
            if (numbers == null || !numbers.Any())
                return 0;

            // Sort the decimal array
            Array.Sort(numbers);

            decimal medianValue = 0;

            var count = numbers.Length;
            if (count % 2 == 0)
            {
                // count is even, need to get the middle two elements, add them together, then divide by 2
                var middleFirst = numbers[(count / 2) - 1];
                var middleSecond = numbers[(count / 2)];
                medianValue = (middleSecond + middleFirst) / 2;
            }
            else
            {
                // count is odd, simply get the middle element.
                medianValue = numbers[(count / 2)];
            }

            return medianValue.TruncateForCurrency();
        }
    }
}
