﻿using System;

namespace PokerLeaderboardApp.Extensions
{
    public static class CurrencyExtensions
    {
        public static decimal TruncateForCurrency (this decimal value)
        {
            return Math.Truncate(value * 100) / 100;
        }

        public static bool TryValidateCurrencyValue (this string value, out decimal? decimalValue, decimal lowerLimit, decimal upperLimit)
        {
            try
            {
                var val = decimal.Parse(value);
                
                if ((val * 100) != (Math.Truncate(val * 100)) || val < lowerLimit || val > upperLimit)
                {
                    decimalValue = null;
                    return false;
                }
                decimalValue = val.TruncateForCurrency();

            }
            catch (Exception ex)
            {
                decimalValue = null;
                return false;
            }

            return true;
        }
    }
}
