﻿
using System.Collections.ObjectModel;
using PokerLeaderboardApp.Extensions;

namespace PokerLeaderboardApp.Entity
{
    public class Player
    {
        
        public int Id { get; set; }

        public string Name { get; set; }

        private decimal _winnings;

        public decimal Winnings
        {
            get => _winnings;
            set => _winnings = value.TruncateForCurrency();
        }

        public bool ClosestToMean { get; set; }

        public bool ClosestToMedian { get; set; }
    }
}
