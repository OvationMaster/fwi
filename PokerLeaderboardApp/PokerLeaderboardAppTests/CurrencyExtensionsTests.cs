﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerLeaderboardApp.Extensions;

namespace PokerLeaderboardAppTests
{
    [TestClass()]
    public class CurrencyExtensionsTests
    {
        [TestMethod()]
        public void TruncateForCurrency_BasicRounding_Test()
        {
            Assert.AreEqual(123.45M, 123.456M.TruncateForCurrency());
        }

        [TestMethod()]
        public void TruncateForCurrency_LongDigits_Test()
        {
            Assert.AreEqual(123.32M, 123.3288444M.TruncateForCurrency());
        }

        [TestMethod()]
        public void TruncateForCurrency_ZeroValue_Test()
        {
            Assert.AreEqual(0M, 0M.TruncateForCurrency());
        }

        [TestMethod()]
        public void TruncateForCurrency_DecimalOnly_Test()
        {
            Assert.AreEqual(.56M, .56798M.TruncateForCurrency());
        }

        [TestMethod()]
        public void TruncateForCurrency_NegativeValue_Test()
        {
            decimal d = -230.5679M;
            Assert.AreEqual(-230.56M, d.TruncateForCurrency());
        }

        [TestMethod()]
        public void TryValidateCurrencyValue_LimitsValidation_Tests()
        {
            Assert.IsTrue("123.9".TryValidateCurrencyValue(out decimal? d1, 120M, 200M));
            Assert.IsFalse("-120".TryValidateCurrencyValue(out decimal? d2, 120M, 200M));
            Assert.IsTrue(".91".TryValidateCurrencyValue(out decimal? d3, .90M, 1.00M));
            Assert.IsFalse("1.25".TryValidateCurrencyValue(out decimal? d4, 1.26M, 2.0M));
            Assert.IsFalse("-6.50".TryValidateCurrencyValue(out decimal? d5, 1.26M, 6.0M));
            Assert.IsTrue("-6.50".TryValidateCurrencyValue(out decimal? d6, -10.26M, 0.0M));
            Assert.IsTrue("-100".TryValidateCurrencyValue(out decimal? d7, -100M, 0.0M));
            Assert.IsTrue("125".TryValidateCurrencyValue(out decimal? d8, -10.26M, 125M));
        }

        [TestMethod()]
        public void TryValidateCurrencyValue_NotNumeric_Tests()
        {
            Assert.IsFalse("A123".TryValidateCurrencyValue(out decimal? d1, 120M, 200M));
            Assert.IsFalse("XY".TryValidateCurrencyValue(out decimal? d2, 120M, 200M));
        }

        [TestMethod()]
        public void TryValidateCurrencyValue_DecimalSize_Tests()
        {
            Assert.IsFalse("12.981".TryValidateCurrencyValue(out decimal? d1, 1M, 200M));
            Assert.IsTrue("1.56".TryValidateCurrencyValue(out decimal? d2, 1M, 200M));
            Assert.IsTrue("1.56".TryValidateCurrencyValue(out decimal? d3, 1M, 200M));
            Assert.IsFalse(".45900".TryValidateCurrencyValue(out decimal? d4, -1M, 200M));
        }

        [TestMethod()]
        public void TryValidateCurrencyValue_OutputValue_Tests()
        {

            Assert.IsFalse("12.98".TryValidateCurrencyValue(out decimal? d1, 15M, 100M));
            Assert.IsNull(d1);
            Assert.IsTrue("100.45".TryValidateCurrencyValue(out decimal? d2, 100M, 200M));
            Assert.AreEqual(100.45M, d2);
            Assert.IsTrue("-.4".TryValidateCurrencyValue(out decimal? d3, -1M, 1M));
            Assert.AreEqual(-.40M, d3);
        }
    }
}