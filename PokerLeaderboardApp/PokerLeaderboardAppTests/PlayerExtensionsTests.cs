﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerLeaderboardApp.Entity;
using PokerLeaderboardApp.Extensions;

namespace PokerLeaderboardAppTests
{
    [TestClass()]
    public class PlayerExtensionsTests
    {
        List<Player> playerData1;
        List<Player> playerData2;
        List<Player> playerData3;
        List<Player> playerData4;

        [TestInitialize]
        public void InitializeData() 
        {
            playerData1 = new List<Player>()
            {
                new Player() {Id = 100, Name = "Joe", Winnings = 124.56M },
                new Player() {Id = 200, Name = "Bill", Winnings = 200.45M },
                new Player() {Id = 300, Name = "Mary", Winnings = 1234.00M },
            };
            playerData2 = new List<Player>()
            {
                new Player() {Id = 100, Name = "Joe", Winnings = 124.56M },
                new Player() {Id = 200, Name = "Bill", Winnings = 200.45M },
                new Player() {Id = 300, Name = "Mary", Winnings = 1234.00M },
                new Player() {Id = 500, Name = "Ralph", Winnings = 112.98M },
            };
            playerData3 = new List<Player>()
            {
                new Player() {Id = 100, Name = "Joe", Winnings = 124.56M },
                new Player() {Id = 200, Name = "Bill", Winnings = -200.45M },
                new Player() {Id = 300, Name = "Mary", Winnings = 1234.00M },
                new Player() {Id = 500, Name = "Ralph", Winnings = 112.98M },
            };
            playerData4 = new List<Player>()
            {
                new Player() {Id = 100, Name = "Joe", Winnings = 100.20M },
                new Player() {Id = 200, Name = "Bill", Winnings = 200.40M },
                new Player() {Id = 300, Name = "Mary", Winnings = 300.80M },
                new Player() {Id = 500, Name = "Ralph", Winnings = 400M },
            };
        }

        [TestMethod()]
        public void SetPlayerClosestToMeanTest()
        {
            playerData1.SetPlayerClosestToMean(190M);
            Assert.IsTrue(playerData1[1].ClosestToMean);
            Assert.IsFalse(playerData1[0].ClosestToMean);
            Assert.IsFalse(playerData1[2].ClosestToMean);
        }

        [TestMethod()]
        public void SetPlayerClosestToMean_MultipleMatches_Test()
        {
            playerData4.SetPlayerClosestToMean(250.6M);
            Assert.IsTrue(playerData4[1].ClosestToMean);
            Assert.IsTrue(playerData4[2].ClosestToMean);
            Assert.IsFalse(playerData4[0].ClosestToMean);
            Assert.IsFalse(playerData4[3].ClosestToMean);
        }

        [TestMethod()]
        public void SetPlayerClosestToMedianTest()
        {
            playerData1.SetPlayerClosestToMean(200.45M);
            Assert.IsTrue(playerData1[1].ClosestToMean);
            Assert.IsFalse(playerData1[0].ClosestToMean);
            Assert.IsFalse(playerData1[2].ClosestToMean);
        }

        [TestMethod()]
        public void SetPlayerClosestToMedian_MultipleMatches_Test()
        {
            playerData2.SetPlayerClosestToMean(250.2M);
            Assert.IsTrue(playerData2[1].ClosestToMean);
            Assert.IsFalse(playerData2[0].ClosestToMean);
            Assert.IsFalse(playerData2[2].ClosestToMean);
        }

        [TestMethod()]
        public void GetNextPlayerIdTest()
        {
            var nextId = playerData3.GetNextPlayerId();
            Assert.IsTrue(nextId == 501);
        }

        [TestMethod()]
        public void CalculateMeanTest()
        {
            Assert.AreEqual(519.67M, playerData1.CalculateMean());
            Assert.AreEqual(417.99M, playerData2.CalculateMean());
            Assert.AreEqual(317.77M, playerData3.CalculateMean());
        }

        [TestMethod()]
        public void CalculateMedianTest()
        {
            Assert.AreEqual(200.45M, playerData1.CalculateMedian());
            Assert.AreEqual(162.50M, playerData2.CalculateMedian());
            Assert.AreEqual(118.77M, playerData3.CalculateMedian());
        }
    }
}