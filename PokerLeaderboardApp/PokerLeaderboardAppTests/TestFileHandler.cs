﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokerLeaderboardApp;
using PokerLeaderboardApp.Data;
using PokerLeaderboardApp.Extensions;

namespace PokerLeaderboardAppTests
{
    class TestFileHandler : IFileHandler
    {
        public byte[] BytesWritten { get; set; }

        public bool FileExists(string filePath)
        {
            throw new NotImplementedException();
        }

        public byte[] ReadAllBytes(string filePath)
        {
            throw new NotImplementedException();
        }

        public void WriteAllBytes(string filePath, byte[] data)
        {
            BytesWritten = data;
        }
    }
}
