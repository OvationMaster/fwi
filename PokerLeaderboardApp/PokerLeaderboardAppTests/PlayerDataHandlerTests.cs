﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using PokerLeaderboardApp.Data;
using PokerLeaderboardApp.Entity;
using PokerLeaderboardApp.Repository;

namespace PokerLeaderboardAppTests
{
    [TestClass()]
    public class PlayerDataHandlerTests
    {
        Mock<IFileHandler> fileHandlerMock;
        List<Player> playerData1;
        string playerData1Json;
        TestFileHandler testFileHandler;

        [TestInitialize]
        public void Setup()
        {
            fileHandlerMock = new Mock<IFileHandler>();
            playerData1 = new List<Player>()
            {
                new Player() {Id = 100, Name = "Joe", Winnings = 124.56M },
                new Player() {Id = 200, Name = "Bill", Winnings = 200.45M },
                new Player() {Id = 300, Name = "Mary", Winnings = 1234.00M },
            };
            playerData1Json = JsonConvert.SerializeObject(playerData1);

            testFileHandler = new TestFileHandler();
        }

        [TestMethod]
        public void GetFullDataPath_Test()
        {
            var basePath = "c:" + Path.DirectorySeparatorChar + "sample";
            var resultingFullDataPath = basePath + Path.DirectorySeparatorChar + "PlayerWinnings.json";

            var handler = new PlayerRepository(fileHandlerMock.Object, basePath);
            Assert.AreEqual(handler.FullDataFilePath, resultingFullDataPath);
        }

        [TestMethod()]
        public void InitializeDataTest()
        {
            // verify that WriteAllBytes is called
            fileHandlerMock.Setup(s => s.WriteAllBytes(It.IsAny<string>(), It.IsAny<byte[]>())).Verifiable();
            var handler = new PlayerRepository(fileHandlerMock.Object, "somepath");
            handler.InitializeData();
            fileHandlerMock.Verify();            
        }

        [TestMethod()]
        public void GetDataTest()
        {
            var bytes = Encoding.Default.GetBytes(playerData1Json);
            fileHandlerMock.Setup(s => s.ReadAllBytes(It.IsAny<string>())).Returns(bytes);
            var handler = new PlayerRepository(fileHandlerMock.Object, "somepath");
            var players = handler.GetData();
            var playersJson = JsonConvert.SerializeObject(players);
            Assert.IsTrue(playersJson.Equals(playerData1Json));
        }

        [TestMethod()]
        public void GetDataTest_EmptyData()
        {
            var lst = new List<Player>();
            var json = JsonConvert.SerializeObject(lst);
            var bytes = Encoding.Default.GetBytes(json);
            fileHandlerMock.Setup(s => s.ReadAllBytes(It.IsAny<string>())).Returns(bytes);
            var handler = new PlayerRepository(fileHandlerMock.Object, "somepath");
            var players = handler.GetData();
            var playersJson = JsonConvert.SerializeObject(players);
            Assert.IsTrue(playersJson.Equals(json));
        }

        [TestMethod()]
        public void SaveDataTest_Success()
        {
            var bytes = Encoding.Default.GetBytes(playerData1Json);
            fileHandlerMock.Setup(s => s.WriteAllBytes(It.IsAny<string>(), bytes)).Verifiable();
            var handler = new PlayerRepository(fileHandlerMock.Object, "somepath");
            handler.SaveData(playerData1);
            fileHandlerMock.VerifyAll();
        }

        [TestMethod()]
        public void SaveDataTest_EmptyArray_Success()
        {
            var lst = new List<Player>();
            var json = JsonConvert.SerializeObject(lst);
            var bytes = Encoding.Default.GetBytes(json);
            fileHandlerMock.Setup(s => s.WriteAllBytes(It.IsAny<string>(), bytes)).Verifiable();
            var handler = new PlayerRepository(fileHandlerMock.Object, "somepath");
            handler.SaveData(lst);
            fileHandlerMock.VerifyAll();
        }

        [TestMethod()]
        public void VerifyDataFileExists_True_Test()
        {
            fileHandlerMock.Setup(s => s.FileExists(It.IsAny<string>())).Returns(true);
            var handler = new PlayerRepository(fileHandlerMock.Object, "somedirectory");
            Assert.IsTrue(handler.VerifyDataFileExists());
        }

        [TestMethod()]
        public void VerifyDataFileExists_False_Test()
        {
            fileHandlerMock.Setup(s => s.FileExists(It.IsAny<string>())).Returns(false);
            var handler = new PlayerRepository(fileHandlerMock.Object, "somedirectory");
            Assert.IsFalse(handler.VerifyDataFileExists());
        }

    }
}