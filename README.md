# PokerLeaderboardApp

PokerLeaderboardApp is a .Net Console application which shows leaderboard statistics for a list of players.  Players can be added, edited or removed.  The Mean and Median winnings is shown as well as indicators for which player(s) are closest to the Mean and Median.

### How to Execute
Simply enter: PokerLeaderboardApp.Exe <DataDirectory> from the command line.
The DataDirectory parameter must be a valid Directory/Folder on the local machine.  
NOTE: a data file does not need to exist initially, but the DataDirectory entered must exist.  Once the app is started a data file will be created and maintained in the DataDirectory.

For Example:  you could enter PokerLeaderboardApp c:\temp
#### Notes
Currently winnings entered must be between $1,000,000.00 and -$1,000,000.00 inclusively.    A player name can be duplicated.
